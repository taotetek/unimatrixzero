package main

import (
	"os"

	"github.com/taotetek/unimatrixzero"
)

func main() {
	link := unimatrixzero.NewTranslink("tcp://127.0.0.1:31337")

	err := link.Run()
	if err != nil {
		os.Exit(1)
	}
	os.Exit(0)
}
