package main

import (
	"os"

	"github.com/taotetek/unimatrixzero"
)

func main() {
	matrix := unimatrixzero.NewUniMatrix("./welcome.txt")
	matrix.Listen(os.Args[1])
	os.Exit(0)
}
