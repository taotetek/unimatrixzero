package unimatrixzero

import (
	"time"

	"github.com/nsf/termbox-go"
	"github.com/zeromq/goczmq"
)

type cursor struct {
	x int
	y int
}

type terminfo struct {
	w int
	h int
}

type Translink struct {
	endpoint  string
	baud      int
	sleep     int
	sock      *goczmq.Sock
	channeler *goczmq.Channeler
	curSend   *cursor
	curRecv   *cursor
	termInfo  *terminfo
	eventChan chan termbox.Event
	msgChan   chan []byte
	sendBuf   []byte
}

func NewTranslink(url string) *Translink {
	w, h := termbox.Size()

	t := &Translink{
		endpoint:  url,
		baud:      1200,
		curSend:   &cursor{x: 1, y: 10},
		curRecv:   &cursor{x: 1, y: 1},
		termInfo:  &terminfo{w: w, h: h},
		eventChan: make(chan termbox.Event),
		msgChan:   make(chan []byte),
		sendBuf:   make([]byte, 0),
	}

	t.sleep = 1000 / (t.baud / 10)
	t.sock = goczmq.NewSock(goczmq.DEALER)
	t.channeler = goczmq.NewChanneler(t.sock, false)
	t.channeler.AttachChan <- url

	return t
}

func (t *Translink) redraw() {
	w, h := termbox.Size()
	termbox.SetCursor(t.curSend.x, t.curSend.y)
	termbox.Flush()
	t.termInfo = &terminfo{w: w, h: h}
}

func (t *Translink) send(msg string) {
	t.channeler.SendChan <- [][]byte{[]byte(msg)}
}

func (t *Translink) Run() error {

	err := termbox.Init()
	if err != nil {
		return err
	}

	termbox.SetInputMode(termbox.InputEsc)
	termbox.SetCursor(t.curSend.x, t.curSend.y)

	go func() {
		for {
			switch ev := termbox.PollEvent(); ev.Type {
			case termbox.EventKey:
				t.eventChan <- ev
			}
		}
	}()

	go func() {
		for {
			msg := <-t.msgChan
			for _, char := range msg {
				if char == '\n' {
					t.curRecv.x = 1
					t.curRecv.y++
					t.redraw()
				} else {
					termbox.SetCell(t.curRecv.x, t.curRecv.y, rune(char), 100, 240)
					t.curRecv.x++
					t.redraw()
				}
				time.Sleep(time.Duration(t.sleep) * time.Millisecond)
			}
			t.curRecv.x = 1
			t.curRecv.y++
			t.redraw()
		}
	}()

	t.send("LINK")

	for {
		select {
		case ev := <-t.eventChan:
			switch ev.Key {
			case termbox.KeyEsc:
				goto exitLink
			case termbox.KeyEnter:
				t.send(string(t.sendBuf))
				t.sendBuf = make([]byte, 0)
				t.curSend.x = 1
				for x := 1; x < 80; x++ {
					termbox.SetCell(x, t.curSend.y, rune(' '), 100, 240)
				}
				t.redraw()
			default:
				switch ev.Ch {
				case 0:
					t.sendBuf = append(t.sendBuf, byte(' '))
				default:
					t.sendBuf = append(t.sendBuf, byte(ev.Ch))
				}

				termbox.SetCell(t.curSend.x, t.curSend.y, rune(ev.Ch), 100, 240)
				t.curSend.x++
				t.redraw()
			}
		case msg := <-t.channeler.RecvChan:
			t.msgChan <- msg[0]
		}
	}
exitLink:
	termbox.Close()
	return nil
}
