package unimatrixzero

import (
	"bytes"
	"io/ioutil"
	"time"

	"github.com/zeromq/goczmq"
)

type node struct {
	id         []byte
	lastActive time.Time
}

type UniMatrix struct {
	welcome  []byte
	nodes    map[string]*node
	sock     *goczmq.Sock
	sockChan *goczmq.Channeler
}

func NewUniMatrix(welcome string) *UniMatrix {
	matrix := &UniMatrix{
		nodes: make(map[string]*node),
	}

	matrix.welcome, _ = ioutil.ReadFile(welcome)
	return matrix
}

func (u *UniMatrix) Listen(url string) {
	u.sock = goczmq.NewSock(goczmq.ROUTER)
	u.sockChan = goczmq.NewChanneler(u.sock, false)
	u.sockChan.AttachChan <- url

	for {
		msg := <-u.sockChan.RecvChan
		if _, ok := u.nodes[string(msg[0])]; !ok {
			u.nodes[string(msg[0])] = &node{id: msg[0], lastActive: time.Now()}
			msg[1] = u.welcome
			u.sockChan.SendChan <- msg
		} else {
			for _, v := range u.nodes {
				if !bytes.Equal(v.id, msg[0]) {
					newMsg := make([][]byte, 2)
					newMsg[0] = v.id
					newMsg[1] = msg[1]
					u.sockChan.SendChan <- newMsg
				}
			}
		}
	}
}
